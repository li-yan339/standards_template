# OpenHarmony资料开发流程和要求

- [一、写作流程](#一-写作流程)
- [二、写作要求](#二-写作要求)

## 一、写作流程


社区资料交付需按照以下流程完成各个环节，最终完成中英文上库。

注意： **_API注释写作用中文，将中文.h文件提交到中文注释仓（[存放在zh-cn文件夹](https://gitee.com/openharmony-sig/interface_native_header/tree/master/zh-cn)），然后需在docs仓创建翻译issue ，API注释翻译完成后，将英文.h文件（[存放在en文件夹](https://gitee.com/openharmony-sig/interface_native_header/tree/master/en)）合入库中。_**


**图1** OpenHarmony资料开发流程

![zh-cn_image_0000001227699801](figures/zh-cn_image_0000001227699801.png)

## 二、写作要求


- 资料交付过程中，责任人需要根据表1中的要求审视各环节的完成情况，并输出对应交付件，上一个环节的要求满足后才能进入下一个环节。

- 评审、测试环节可视情况并行处理。

  


**表1** 各交付环节准出要求

| 资料交付环节 | 写作要求 | 交付件 | 执行人 | 是否完成 |
| -------- | -------- | -------- | -------- | -------- |
| 写作 | 1.&nbsp;指南和API参考分别按照 **_[指南模板](https://gitee.com/duangavin123/standards_template/blob/master/%E5%BC%80%E5%8F%91%E6%8C%87%E5%8D%97%E5%86%99%E4%BD%9C%E6%A8%A1%E6%9D%BF.md)_** 和 _**[API模板规范](https://gitee.com/duangavin123/standards_template/blob/master/C&C++%20API%E6%B3%A8%E9%87%8A%E8%A7%84%E8%8C%83.md)**_ 完成写作，图形配色按照 **_[图形配色规范](https://gitee.com/duangavin123/standards_template/blob/master/OpenHarmony%E5%BC%80%E5%8F%91%E6%8C%87%E5%8D%97%E5%9B%BE%E5%BD%A2%E9%A2%9C%E8%89%B2%E8%A7%84%E8%8C%83.pptx)_** 绘制（新增文档需要按照模板写作，增量更新直接基于原有文档更新） | 资料初稿 | 作者 |  |
|  | 2.&nbsp;按照 **_[开发指南质量评估标准](https://gitee.com/duangavin123/standards_template/blob/master/%E8%B4%A8%E9%87%8F%E8%AF%84%E4%BC%B0%E6%A0%87%E5%87%86-%E5%BC%80%E5%8F%91%E6%8C%87%E5%8D%97.md)_** 和 **_[API质量评估标准](https://gitee.com/duangavin123/standards_template/blob/master/%E8%B4%A8%E9%87%8F%E8%AF%84%E4%BC%B0%E6%A0%87%E5%87%86_API_C&C++.md)_** 完成自检 | NA | 作者 |  |
|  | 3.&nbsp;提交中文PR到文档所在仓 | 中文PR链接 | 作者 |  |
| 评审 | 1.&nbsp;SE、生态、资料等关键评审角色进行评审，直接在PR上添加评审意见 | NA | 作者、资料、生态等关键角色 |  |
|  | 2.&nbsp;所有意见处理闭环，评审人在PR评论区回复评审通过，SE评论“ **TechApprove** ”，资料评论“ **DocApprove** ” | 关键角色在PR评论区的通过评论 | SE、资料 |  |
| 测试 | 1.&nbsp;资料随迭代一并转测试，按照模板提交给测试人员 | 转测试邮件 | 作者 |  |
|  | 2.&nbsp;测试人员进行测试，通过issue记录测试问题 | 问题issue | 测试 |  |
|  | 3.&nbsp;所有测试意见处理闭环 | NA | 作者 |  |
|  | 4.&nbsp;测试人员在测试报告中给出测试通过结论（在已有内容基础上增量修改的，如果没有对应的迭代需求，测试通过后，测试人员在对应PR下评论“ **TestApprove** 即可”） | 测试通过结论 | 测试 |  |
| 合规 | 1.&nbsp;完成社区dco签署 | dco检查通过 | 作者 |  |
|  | 2. 处理CI问题（社区每日构建看板） | NA | 作者 | |
| 翻译 | 1. 提交翻译，&nbsp;docs仓会自动创建翻译issue，其他仓需要作者按照模板手动创建翻译issue并分配责任人给周云 | issue链接 | 翻译 |  |
|  | 2.&nbsp;完成对译文的技术校验 | NA | 作者 |  |
|  | 3.&nbsp;翻译人员提交中英文PR到docs仓，对应仓committer合并PR | 英文PR链接 | 翻译 |  |
